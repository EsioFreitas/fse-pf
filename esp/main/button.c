#include "./inc/button.h"

xQueueHandle filaDeInterrupcao;

int clickBotao = 0;

static void IRAM_ATTR gpio_isr_handler(void *args)
{
    int pino = (int)args;
    xQueueSendFromISR(filaDeInterrupcao, &pino, NULL);
}

void inicializaBotao()
{
    filaDeInterrupcao = xQueueCreate(10, sizeof(int));
    gpio_pad_select_gpio(BOTAO);
    gpio_set_direction(BOTAO, GPIO_MODE_INPUT);
    gpio_pulldown_en(BOTAO);
    gpio_pullup_dis(BOTAO);
    gpio_set_intr_type(BOTAO, GPIO_INTR_POSEDGE);

    gpio_install_isr_service(0);
    gpio_isr_handler_add(BOTAO, gpio_isr_handler, (void *)BOTAO);
}

void trataInterrupcaoBotao(void *params)
{
    int pino;
    while (true)
    {
        if (xQueueReceive(filaDeInterrupcao, &pino, portMAX_DELAY))
        {
            int estado = gpio_get_level(pino);
            printf("estado = %d\n", estado);
            if (estado == 1)
            {
                printf("estado = 1\n");
                gpio_isr_handler_remove(pino);
                clickBotao = !clickBotao;

                mandaMensagemEstado();
                vTaskDelay(50 / portTICK_PERIOD_MS);
                gpio_isr_handler_add(pino, gpio_isr_handler, (void *)pino);
            }
        }
    }
}