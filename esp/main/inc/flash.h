#ifndef FLASH_H_
#define FLASH_H_

#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"

int le_valor_nvs();
void grava_valor_nvs(char* var_name);
void erase_nvs();

#endif