#include "./inc/createsJson.h"

extern char topicoComodo[300];
extern int estadoLed;
extern int clickBotao;
extern char *macAddress;

int createsJson(cJSON *espInfo, cJSON *titulo, char nome[], float info)
{

    titulo = cJSON_CreateNumber(info);
    if (titulo == NULL)
    {
        ESP_LOGE("JSON", "JSON ERROR\n");
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        return 1;
    }

    cJSON_AddItemToObject(espInfo, nome, titulo);
    return 0;
}

int createsJsonStr(cJSON *espInfo, cJSON *titulo)
{
    titulo = cJSON_CreateString(macAddress);
    if (titulo == NULL)
    {
        ESP_LOGE("JSON", "JSON ERROR\n");
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        return 1;
    }
    cJSON_AddItemToObject(espInfo, "id", titulo);
    return 0;
}

void mandaMensagem(char *topico, float info)
{
    cJSON *json = cJSON_CreateObject();
    while (json == NULL)
    {
        cJSON_Delete(json);
        ESP_LOGE("JSON", "JSON ERROR\n");
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        json = cJSON_CreateObject();
    }

    cJSON *mensagem = NULL;
    while (createsJsonStr(json, mensagem));
    while (createsJson(json, mensagem, topico, info));

    char *info2 = cJSON_Print(json);
    char enviaEstado[500];
    sprintf(enviaEstado, "%s/%s", topicoComodo, topico);
    mqtt_envia_mensagem(enviaEstado, info2);
    cJSON_Delete(json);
}

void mandaMensagemEstado()
{
    cJSON *json = cJSON_CreateObject();
    while (json == NULL)
    {
        cJSON_Delete(json);
        ESP_LOGE("JSON", "JSON ERROR\n");
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        json = cJSON_CreateObject();
    }

    cJSON *id = NULL;
    while (createsJsonStr(json, id));

    cJSON *toggle_device = NULL;
    while (createsJson(json, toggle_device, "toggle_device", !estadoLed));

    cJSON *entrada = NULL;
    while (createsJson(json, entrada, "entrada", clickBotao));

    char *info = cJSON_Print(json);
    char enviaEstado[500];
    sprintf(enviaEstado, "%s/estado", topicoComodo);
    printf("mandaMensagemEstado = %s\n", enviaEstado);
    printf("msg = %s\n", info);
    mqtt_envia_mensagem(enviaEstado, info);
    cJSON_Delete(json);
}