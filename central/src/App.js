import {
  Button,
  HStack,
  Box,
  Text,
  Heading,
  useDisclosure,
} from "@chakra-ui/react";

import downloadCsv from "download-csv";
import { format } from "date-fns";
import { useEffect, useState } from "react";
import audio from "./alarm.mp3";
import { useFormik } from "formik";

import { ROOMS } from "./data";
import Card from "./component/card";
import AlertModal from "./component/alertModal";
import LogModal from "./component/logModal";
import DeviceModal from "./component/deviceModal";
import mqtt from "mqtt";

const BASE_TOPIC = "fse2021/170129411";
let client;

function App() {
  const [isAlarmTurnOn, setIsAlarmTurnOn] = useState(false);

  const [data, setData] = useState({
    // "[id]": {
    //   id: "[id]",
    //   comodo: "Sala",
    //   temperatura: 0,
    //   umidade: 0,
    //   sensor: 0,
    //   entrada: "values.entrada",
    //   saida: "values.saida",
    // },
  });
  const [logs, setLogs] = useState([]);

  const deviceModel = useDisclosure();
  const logModel = useDisclosure();
  const alertModel = useDisclosure();

  const [toRegister, setToRegister] = useState([]);

  const formik = useFormik({
    initialValues: {
      comodo: "",
      entrada: "",
      saida: "",
      tocar: false,
    },

    onSubmit: (values) => {
      const comodo = ROOMS.find((el) => el.id === values.comodo).id;
      const id = toRegister[0];
      const tempToRegiuster = [...toRegister];
      tempToRegiuster.splice(0, 1);
      setToRegister(tempToRegiuster);

      sendMqttMsg(`${BASE_TOPIC}/dispositivos/${id}`, {
        command: "register",
        comodo,
        // entrada: values.entrada,
        // saida: values.saida
      });

      setData((prevState) => ({
        ...prevState,
        [id]: {
          id,
          comodo,
          temperatura: 0,
          umidade: 0,
          sensor: 0,
          entrada: values.entrada,
          saida: values.saida,
        },
      }));

      saveLogMessage(`Dispositivo ${id} cadastrado`);

      localStorage.setItem(
        "data",
        JSON.stringify({
          ...data,
          [id]: {
            id,
            comodo,
            temperatura: 0,
            umidade: 0,
            sensor: 0,
            tocar: values.tocar,
            entrada: values.entrada,
            saida: values.saida,
          },
        })
      );

      formik.resetForm();
      deviceModel.onClose();
    },
  });

  function sendMqttMsg(topic, content) {
    if (client) client.publish(topic, JSON.stringify(content));
  }

  useEffect(() => {
    const localLog = JSON.parse(localStorage.getItem("log"));
    setLogs(localLog ?? []);
  }, []);

  useEffect(() => {
    client = mqtt.connect("mqtt://broker.hivemq.com:8000/mqtt");

    const localData = JSON.parse(localStorage.getItem("data"));
    setData({ ...data, ...localData });

    client.subscribe(`${BASE_TOPIC}/#`);
    client.on("message", (topic, message) => {
      const messageJSON = JSON.parse(message.toString());
      const deviceID = messageJSON.id;
      // console.log(topic);
      if (topic.split("/").slice(-2, -1)[0] === "dispositivos") {
        if (deviceID && !Object.keys(data).includes(deviceID)) {
          setToRegister([...toRegister, deviceID]);
        }
      } else if (topic.split("/").pop() === "temperatura") {
        setData((prevState) => ({
          ...prevState,
          [deviceID]: {
            ...prevState[deviceID],
            temperatura: messageJSON.temperatura,
          },
        }));
      } else if (topic.split("/").pop() === "umidade") {
        setData((prevState) => ({
          ...prevState,
          [deviceID]: { ...prevState[deviceID], umidade: messageJSON.umidade },
        }));
      } else if (topic.split("/").pop() === "estado") {
        if (messageJSON.entrada) {
          alertUser();
        }
        // setData((prevState) => ({
        //   ...prevState,
        //   [deviceID]: {
        //     ...prevState[deviceID],
        //     sensor: !prevState[deviceID].saida,
        //   },
        // }));
        // console.log(messageJSON);
      }
    });
  }, []);

  const saveLogMessage = (message) => {
    console.log(logs);
    setLogs([
      ...logs,
      {
        message: message,
        time: format(new Date(), "yyyy-MM-dd 'às' HH:mm:ss"),
      },
    ]);

    localStorage.setItem(
      "log",
      JSON.stringify([
        ...logs,
        {
          message: message,
          time: format(new Date(), "yyyy-MM-dd 'às' HH:mm:ss"),
        },
      ])
    );
  };

  const alertUser = () => {
    const alarm = new Audio(audio);
    saveLogMessage("Alarme acionado");
    alertModel.onOpen();

    alarm.play();
  };

  const toggleAlarm = () => {
    const message = isAlarmTurnOn
      ? "Alarme foi desligado"
      : "Alarme foi ligado";
    saveLogMessage(message);
    setIsAlarmTurnOn(!isAlarmTurnOn);
  };

  const toggleDevice = (device) => {
    const message = device.sensor
      ? `Device ${device.id} foi desligado`
      : `Device ${device.id} foi ligado`;

    sendMqttMsg(`${BASE_TOPIC}/dispositivos/${device.id}`, {
      toggle_device: !data[device.id].sensor ? 1 : 0,
    });

    setData((prevState) => ({
      ...prevState,
      [device.id]: {
        ...prevState[device.id],
        sensor: !data[device.id].sensor,
      },
    }));

    saveLogMessage(message);
  };

  const exportLog = () => {
    downloadCsv(
      logs,
      { message: "Mensagem", time: "hora" },
      "logProjetoFinal.csv"
    );
    logModel.onClose();
  };

  return (
    <Box pb="1rem">
      <Box bgColor="blue.500" color="white" p="1rem 2rem">
        <Heading>Trabalho Final FSE </Heading>
      </Box>
      <Box mx="2rem">
        <HStack justifyContent="space-between" my="2rem">
          <Text
            fontSize="xl"
            fontWeight="bold"
            color={!isAlarmTurnOn ? "red" : "green"}
          >
            {isAlarmTurnOn ? "Alarme ligado" : "Alarme desligado"}
          </Text>
          <div>
            <Button
              colorScheme={isAlarmTurnOn ? "red" : "green"}
              mr="1rem"
              variant="outline"
              onClick={toggleAlarm}
            >
              {isAlarmTurnOn ? "Desligar Alarme" : "Ligar Alarme"}
            </Button>
            <Button
              colorScheme="blue"
              mr="1rem"
              variant="outline"
              onClick={logModel.onOpen}
            >
              Log
            </Button>
            <Button
              colorScheme="blue"
              onClick={deviceModel.onOpen}
              disabled={!toRegister.length}
            >
              {toRegister.length
                ? "Cadastrar novo Dispositivo"
                : "Sem novos dispositivos"}
            </Button>
          </div>
        </HStack>
        <DeviceModal deviceModel={deviceModel} formik={formik} rooms={ROOMS} />
        <LogModal logModel={logModel} logs={logs} exportLog={exportLog} />
        <AlertModal alertModel={alertModel} />
        <div>
          {Object.keys(data)?.map((keyName) => (
            <Card
              device={data[keyName]}
              key={keyName}
              toggleDevice={() => toggleDevice(data[keyName])}
            />
          ))}
        </div>
      </Box>
    </Box>
  );
}

export default App;
