import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from "@chakra-ui/react";

function LogModal({ logModel, logs, exportLog }) {
  return (
    <Modal isOpen={logModel.isOpen} onClose={logModel.onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Log</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Table variant="striped" colorScheme="blue" size="sm">
            <Thead>
              <Tr>
                <Th>Ação</Th>
                <Th>Hora</Th>
              </Tr>
            </Thead>
            <Tbody>
              {logs.length ? (
                logs?.map((log, i) => (
                  <Tr key={i}>
                    <Td>{log.message}</Td>
                    <Td>{log.time}</Td>
                  </Tr>
                ))
              ) : (
                <Tr>
                  <Td>Sem log no momento</Td>
                  <Td>Sem log no momento</Td>
                </Tr>
              )}
            </Tbody>
          </Table>
        </ModalBody>

        <ModalFooter>
          <Button
            colorScheme="blue"
            disabled={!logs.length}
            onClick={exportLog}
          >
            Baixar
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default LogModal;
