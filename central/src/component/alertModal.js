import {
  Button,
  Heading,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from "@chakra-ui/react";

function AlertModal({ alertModel, turnOffSensor }) {
  return (
    <Modal isOpen={alertModel.isOpen} onClose={alertModel.onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Alerta</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Heading textAlign="center" color="red.400">
            Alguém entrou na casa
          </Heading>
        </ModalBody>

        <ModalFooter>
          <Button
            colorScheme="blue"
            onClick={() => {
              turnOffSensor();
              alertModel.onClose();
            }}
          >
            Certo
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default AlertModal;
