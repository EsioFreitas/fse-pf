import { Button, HStack, Box, Tag, Text } from "@chakra-ui/react";

function Card({ device, toggleDevice }) {
  return (
    <Box
      bgColor="white"
      p="1.3rem"
      mb="1rem"
      borderRadius="16px"
      boxShadow="3px 3px 3px #35353550"
    >
      <HStack justifyContent="space-between">
        <div>
          <Text fontSize="xl">
            {device && device["id"]
              ? `Dispositivo Id - ${device["id"]}`
              : "Não informado"}
          </Text>
          <div>
            <Tag colorScheme={device && device["sensor"] ? "green" : "red"}>
              {device && device["sensor"] ? "Ligado" : "Desligado"}
            </Tag>
          </div>
        </div>
        <div>
          <Button
            colorScheme={device && device["sensor"] ? "red" : "green"}
            onClick={() => toggleDevice(device.id)}
          >
            {device && device["sensor"] ? "Desligar" : "Ligar"}
          </Button>
        </div>
      </HStack>

      <HStack mt="2rem">
        <Text fontSize="xl" pr="1rem">
          <b>Umidade:</b>{" "}
          {device && device["umidade"]
            ? `${device["umidade"]}%`
            : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Temperatura:</b>{" "}
          {device && device["temperatura"]
            ? `${device["temperatura"]}°C`
            : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Local:</b>{" "}
          {device && device["comodo"] ? device["comodo"] : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Entrada:</b>{" "}
          {device && device["entrada"] ? device["entrada"] : "Não informado"}
        </Text>
        <Text fontSize="xl" pr="1rem">
          <b>Saída:</b>{" "}
          {device && device["saida"] ? device["saida"] : "Não informado"}
        </Text>
      </HStack>
    </Box>
  );
}

export default Card;
