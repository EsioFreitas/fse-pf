import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Stack,
  Select,
  Input,
  Checkbox,
} from "@chakra-ui/react";

function DeviceModal({ deviceModel, formik, rooms }) {
  return (
    <Modal isOpen={deviceModel.isOpen} onClose={deviceModel.onClose} size="xl">
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Cadastrar Dispositivo</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Stack spacing={3}>
            <Select
              id="comodo"
              name="comodo"
              onChange={formik.handleChange}
              value={formik.values.comodo}
              placeholder="Comodo do dispositivo"
            >
              {rooms.map(({ id, name }) => (
                <option value={id} key={id}>
                  {name}
                </option>
              ))}
            </Select>

            <Input
              id="entrada"
              name="entrada"
              onChange={formik.handleChange}
              value={formik.values.entrada}
              placeholder="Entrada do dispositivo"
            />
            <Input
              id="saida"
              name="saida"
              onChange={formik.handleChange}
              value={formik.values.saida}
              placeholder="Saída do dispositivo"
            />
            <Checkbox
              id="tocar"
              name="tocar"
              onChange={formik.handleChange}
              value={formik.values.tocar}
            >
              Tocar o alarme
            </Checkbox>
          </Stack>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" onClick={formik.handleSubmit}>
            Salvar
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}

export default DeviceModal;
