# FSE - Projeto Final

AA ideia do projeto é construir uma automação residencial. Dessa forma, usaa-se uma ESP32 como dispositivo de automação. O led da ESP representando um dispositivo como uma lâmpada e o botão representando um sensor de presença. Além disso, o servidor central que se comunica com a ESP através do protocolo MQTT. Na interface desse servidor central é possivel interagir com os dispositivos cadastrados e acompanhar a temperatura e a humidade sendo medida pela esp, em conjunto com o estado atual do botão. Por fim, existe também uma funcionalidade atual de alarme, que quando armado dispara um som caso o sensor de presença seja ativado.

### Alunos: Ésio Gustavo Pereira Freitas - 170033066 / Guilherme Mendes Pereira - 170129411

## Execução

- **Linguagem:** C na ESP e JavaScript no Central
- **Instruções de execução:**

1. Clonar o repositório
2. Iniciar o servidor central por meio do docker-compose
3. Iniciar a ESP com o código na pasta "esp"
4. Em poucos segundos aparecerá no frontend a solicitação de cadastro da ESP e os menus de interação

## Instruções para iniciar o servidor central

Vá até a pasta `central` e executar um `docker-compose up --build`. Após isso, a aplicação estará acessível na porta 3000 do seu localhost

## Instruções para iniciar a esp

### Clone do repositório

`git clone https://gitlab.com/EsioFreitas/fse-pf`

### Vá para o diretório do projeto do servidor central

`cd fse-pf/esp`

### Exporte os arquivos de configuração da ESP para essa pasta

`. /home/user/esp-idf/export.sh`

### Realize a configuração do menu

`idf.py menuconfig`

### Realize o build com a limpeza de dados

`idf.py build fullclean`

### Limpa memória flash armazenada na ESP

`idf.py -p /dev/ttyUSB0 erase_flash `

### Realize a execução com o monitor

`idf.py -p /dev/ttyUSB0 flash monitor`

## Video demonstrativo

https://youtu.be/UekO0oylc5c
